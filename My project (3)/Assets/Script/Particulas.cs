using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particulas : MonoBehaviour
{
    
    public ParticleSystem insertsystem;

    public void acelerar()
    {
        var main=insertsystem.main;
        main.startSpeed=25;
    }
    public void acelerarFuego()
    {
        var main=insertsystem.main;
        main.startSpeed=2;
    }

    public void cantidad()
    {
        var emission=insertsystem.emission;
        emission.rateOverTime=110;
    }
    public void color()
    {
        var main=insertsystem.main;
        main.startColor=Color.red;
    }
    public void colorDos()
    {
        var main=insertsystem.main;
        main.startColor=Color.green;
    }
    public void colorTres()
    {
        var main=insertsystem.main;
        main.startColor=Color.blue;
    }
}
